Apothecary
==========

This is the [OpenFrameworks](http://openframeworks.cc) library apothecary. It mixes formulas and potions to build and update the C/C++ lib dependencies.

[**Apothecary has moved to openFrameworks core**](https://github.com/openframeworks/openFrameworks/tree/master/scripts/apothecary) in `scripts/apothecary`

2014 OpenFrameworks team  
2013 Dan Wilcox <danomatika@gmail.com> supported by the CMU [Studio for Creative Inquiry](http://studioforcreativeinquiry.org/)
